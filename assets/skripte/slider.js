"use strict";

/* Deklaration (3x)
-> Elemente werden in einer Liste gespeichert
-> Aus den Listen werden Arrays erstellt
-> Speicherung des Sliders in eine Variable
*/

const recipesListone = document.getElementsByClassName('sliderElementone');
const recipesarrayone = Array.from(recipesListone);

const recipesListtwo = document.getElementsByClassName('sliderElementtwo');
const recipesarraytwo = Array.from(recipesListtwo);

const recipesListthree = document.getElementsByClassName('sliderElementthree');
const recipesarraythree = Array.from(recipesListthree);

const slider = document.getElementById("slider");

/* Testfälle abfangen -> keine Empfehlungen dürfen ausgegeben werden
1) keine Rezepte
2) weniger als 3
*/

if (recipesarrayone.length == 0) {
console.log("Gar keine Rezepte am Start");
}

if (recipesarrayone.length > 1 && recipesarrayone.length < 3) {
  console.log("Zu wenige Rezepte sorry");
}

/* erst ab 3 Rezepte dürfen Empfehlungen gezeigt werden */
/* nutzung von "flex", damit die Rezepte Arrays nebeneinander dargestellt werden können */
if (recipesarrayone.length >= 3){
  slider.style.display = "flex";
}


/* STACKING */

/* Initialisierung für die richtige Reihenfolge! */
var a = 0;
var b = 1;
var c = 2;

/* Gibt jedem Element/Bild jedes Arrays den z-Index: 0 für Unsichtbarkeit */
/* Nutzung von opacity: 0 = keinerlei Deckkraft */
for (const element of recipesarrayone) {
  element.style.zIndex = 0;
  element.style.opacity = 0;
}

for (const element of recipesarraytwo) {
  element.style.zIndex = 0;
  element.style.opacity = 0;
}

for (const element of recipesarraythree) {
  element.style.zIndex = 0;
  element.style.opacity = 0;
}

/* -> Initialiserung der Arrays auf die richtige Reihenfolge
  -> Hochzählen der Zählervariablen um bei der Funktion unten weiterzugehen
 */

recipesarrayone[a].style.zIndex = 1;
recipesarrayone[a].style.opacity = 1;
a++;

recipesarraytwo[b].style.zIndex = 1;
recipesarraytwo[b].style.opacity = 1;
b++;

recipesarraythree[c].style.zIndex = 1;
recipesarraythree[c].style.opacity = 1;
c++;



/* SLIDING */

/* Deklaration der Gesamtfunktion:
-> Aufruf der Intervalfunktion mit Einstellung auf 3 Sekunden für die drei Sliding Funktionen (werden auch aufgerufen)
*/
function intervallslide () {

  setInterval(function () {

    /* Zuweisung des neuen Index durch das neue Ergebnis der Funktion stackslide des jeweiligen Arrays */
    a = stackslide (recipesarrayone, a);
    b = stackslide (recipesarraytwo, b);
    c = stackslide (recipesarraythree, c);

  }, 3000);
}

/* Funktionsaufruf der Gesamtfunktion zum Beginn des Slidings */
intervallslide();

/* Funktion zum eigentlichen Sliding */

function stackslide (recipesarray, zaehler) {
  /* Sichtbarkeit des derzeitigen Elements */
  /* Nutzung von opacity: 100% Deckraft*/
  recipesarray[zaehler].style.zIndex = 1;
  recipesarray[zaehler].style.opacity = 1;


  /* Abfangen unterschiedlicher Fälle: */

  if (zaehler != 0) {
    /* Nicht Anfang der Liste: Unsichtbarkeit des vorherigen Elements */
    recipesarray[zaehler - 1].style.zIndex = 0;
    recipesarray[zaehler - 1].style.opacity = 0;

  } else {
    /* Anfang der Liste: Unsichtbarkeit des letzten Elements */
    recipesarray[recipesarray.length - 1].style.zIndex = 0;
    recipesarray[recipesarray.length - 1].style.opacity = 0;
  }

  /* Ende der Liste (Vergleich mit der Länge des Arrays): Null - Initialisierung für Neustart */
  if (zaehler != 0 && zaehler + 1 == recipesarray.length) {
    zaehler = 0;
  } else {
    /* Nicht Ende der Liste: Zaehler hochzählen um weiter zu gehen */
    zaehler++;
  }

  /* Ergebnis der Funktion: neuer Index */
  return zaehler;
}
