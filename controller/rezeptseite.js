'use strict';
var fhWeb = require('fhw-web');

/* Deklaration der Funktion rezept für die Detailseite:
 -> Speicherung des Inhalts von produktdaten.json mit load.Json
  -> es wird mit einem return das genaue Rezept ausgegeben
*/
module.exports = {
  "rezept": function(data) {
    const rezeptdata = fhWeb.loadJson("produktdaten");

    const rezept = rezeptdata.rezepte.filter(function(rezept){
      return(rezept.bildname === data.request.path.id);
    });

    return {
      /* Deklariert auf welcher Seite das Ergebnis der Funktion ausgegeben wird */
      "page":"detail",
      /* Zuweisung in data was genau ausgegeben wird: Das bestimmte Objekt(Rezept) im array rezept */
      "data": {
        "rezept": rezept[0]
      }
    }
  }
}
