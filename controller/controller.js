'use strict';
var fhWeb = require('fhw-web');

/* Deklaration der Funktion rezeptuebersicht für das Gitter:
 -> Speicherung des Inhalts von produktdaten.json mit load.Json
 -> es werden mit einem return die Rezepte aus produktdaten.json zurückgegeben
 */


module.exports = {
  "rezeptuebersicht": function(data) {
    const jsondata = fhWeb.loadJson("produktdaten");

    return {
      /* Deklariert auf welcher Seite das Ergebnis der Funktion ausgegeben wird */
      "page":"gitter",
      /* Zuweisung in data was genau ausgegeben wird */
      "data":{
        "rezeptuebersicht": jsondata.rezepte
      }
    }
  }
}
